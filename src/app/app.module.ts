import { BrowserModule } from '@angular/platform-browser';

import { NgModule, ErrorHandler,NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {APIService} from './Provider/api.service';
import { AppComponent } from './app.component';
import { HttpModule } from '@angular/http';
import {ListComponent} from './ListComponent/list.component';
import { AgGridModule } from 'ag-grid-angular';
@NgModule({
  declarations: [
    AppComponent,
    ListComponent
  ],
  exports: [ ListComponent ],
  imports: [
    BrowserModule,
    HttpModule,
    AgGridModule.withComponents([])
  ],
  providers: [APIService],
  bootstrap: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
