import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable, Subject } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
interface pagelist {
    datalist: string[];
}
@Injectable()
export class APIService {


    // Define private properties
  
    private BaseUrl = 'http://localhost:2017/public/';
  
    headers: Headers;
    options: RequestOptions;

    constructor(private _http: Http) {
        this.headers = new Headers({
            'Content-Type': 'application/json',
            'Accept': 'q=0.8;application/json;q=0.9'
        });
        this.options = new RequestOptions({ headers: this.headers });
    }
   

    get(config) {
        if (!config) return Observable.throw("Error : Null Parameters");
        let updatedURL = this.buildAbsoulteURLnew(config.uri);
        let appendParamToURL = this.getComplexObjectAsQueryString(updatedURL,config);
        return this._http.get(appendParamToURL)
            .map((res: Response) => {
                let responseData = res.json();
                if (responseData) {
                    return responseData;
                } else {
                    return false;
                }
            })
            .catch((error: any) => Observable.throw((error)));
        // .catch(this.handleError);
    }


   
    private getComplexObjectAsQueryString(url,config) {
        let appendParamToURL =url;
        try {
            let params: URLSearchParams = new URLSearchParams();
            for (var key in config.params) {
                if (config.params.hasOwnProperty(key)) {
                    let val = config.params[key];
                    appendParamToURL =appendParamToURL + '/' +key +'=' +val
                    //arams.set(key, val);
                }
            }

            return appendParamToURL ;
        }
        catch (e) {

        }

    }
    extractParams(params: any) {
        if (params) {
            let id = "/" + params;
            return id;
        } else {
            return "";
        }

    }
    buildAbsoulteURL(uri: any, params: any = null) {
        let updatedURL = this.BaseUrl + uri + this.extractParams(params);
        return updatedURL;
    }

    buildAbsoulteURLnew(uri: any, params: any = null) {
        let updatedURL = this.BaseUrl + uri;// + this.extractParams(params);
        return updatedURL;
    }
    private extractData(res: Response) {
        let body = res.json();
        return body || {};
    }

    private handleError(error: any) {
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        return Observable.of(errMsg);
    }
}