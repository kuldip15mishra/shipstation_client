import {Component }from '@angular/core'; 
import {APIService}from '../Provider/api.service'; 
@Component( {
  selector:'orders-list', 
  templateUrl:'./list.component.html', 
  styleUrls:['./list.component.css']
})
export class ListComponent {
  title = 'app'; 
  rowData = []; 
  originalCopy =[];
  paginationPageSize = 0; 
  constructor(private service:APIService
    ) {

      this.loadData(); 

    }
  columnDefs = [ {headerName:'ID', field:'orderId'},  {headerName:'Name', field:'name'},  {headerName:'Amount', field:'shippingAmount'},  {headerName:'Customer User', field:'customerUsername'}
]; 

onSearch(event) {

  this.rowData = this.filterBySearchKey(event.target.value); 
}

filterBySearchKey(searchKey) {
  if(searchKey)
   {return  this.rowData.filter(item =>  {
     return item.name.toString().toUpperCase().indexOf(searchKey.toString().toUpperCase()) >= 0
   })}else{
    return this.originalCopy
   }

   
}
mapToModel(data) {
return data.map(item =>  {
  return {
    orderId:item.orderId, 
    name:item.billTo.name, 
    shippingAmount:item.shippingAmount, 
    customerUsername:item.customerUsername, 
  }
})
}

loadData() {
  let config =  {
  uri:'orders', 
  params: {page:1, pageSize:10}, 
  data:null, 
  responseData:null
}
  return this.service.get(config).subscribe(res =>  {
    let result; 
      if (res) {
        
        this.rowData = this.mapToModel(res.orders.orders)
        this.originalCopy=this.rowData ;
        this.paginationPageSize = res.orders.total; 
      }

      return result; 
  }); 
}
}
